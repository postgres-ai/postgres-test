FROM postgres:11

RUN apt-get update
RUN apt-get -y install postgresql-plpython3-11
RUN apt-get install -y python3-pip
RUN pip3 install requests
